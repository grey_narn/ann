#!/bin/bash
data_set_file_name='data_set_3'
query_set_file_name='query_1'
num_of_points=`wc -l $data_set_file_name| awk ' { print $1 }'`
echo "num_of_points = $num_of_points\n"
./ann_test <<HERE_DOC_ENDING
output_label test-run-0
validate on
dim 2
data_size $num_of_points
# distribution uniform
# gen_data_pts
read_data_pts $data_set_file_name
read_query_pts $query_set_file_name
bucket_size 1
split_rule suggest
shrink_rule none
build_ann
run_queries standard
#delete_point 2
#delete_point 4
#delete_point 3
HERE_DOC_ENDING
